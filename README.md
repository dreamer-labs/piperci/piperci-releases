# piperci-releases

## Introduction

Official release definitions for PiperCI are documented in this repository inside of `releases.yml`.

This file contains a YAML dictionary called `releases` with every release of PiperCI.

The official installer for each release uses parts of this file to determine which versions of which components comprise each official release.

## Releases

### Release Codename

The `releases` dictionary contains one dictionary per release codename, using the release's codename as the dictionary name.

```
---
releases:
  release_name_here:
...
```

Releases are named alphabetically after some of our favorite things. :-)

```
---
releases:
  altstadt:
...
```

Each of these release codename dictionaries contain two sub-dictionaries. `info` and `components`.

### Info

The `info` dictionary in each release codename dictionary contains metadata about the release.

```
---
releases:
  altstadt:
    info:
...
```

#### Number

The `number` variable contains an integer with the numerical release number associated with the release codename.

```
---
releases:
  altstadt:
    info:
      number: 1
...
```

#### Released

The `released` variable contains a boolean. If true, the release is stable. If false, the release is a work-in-progress.

```
---
releases:
  altstadt:
    info:
      number: 1
      released: false
...
```

#### Components

The `components` subdictionary contains information about each component of the release. This includes things like `installer` and `docs`, which each having their own configuration files within each release's subdirectory within this repository.

```
---
releases:
  altstadt:
    info:
      number: 1
      released: false
    components:
      installer:
        uri: "git@gitlab.com:dreamer-labs/piperci/piperci-releases.git"
        version: "v1.0.0"
        config: "altstadt/installer.yml"
      docs:
        uri: "git@gitlab.com:dreamer-labs/piperci/piperci.dreamer-labs.net.git"
        version: "v1.0.0"
        config: "altstadt/docs.yml"
...
```

#### Installer Config File

The value of `releases.${release_name_here}.components.installer.config` is a file containing configuration data feed into the installer located at `uri` with version `version`. The `installer` dictionary in this file lists each component that the installer downloads and installs along with instructions for the installer on how to retrieve it. The installer is `ansible` based, and therefore uses `args` passed to different ansible modules defined within the installer repository. The installer may pull from source code, docker repositories, or other sources.

```
---
installer:
  picli:
    args:
      uri: "git@gitlab.com:dreamer-labs/piperci/piperci-picli.git"
      version: "master"
  gman:
    args:
      uri: "git@gitlab.com:dreamer-labs/piperci/piperci-gman.git"
      version: "master"
  libs:
    args:
      uri: "git@gitlab.com:dreamer-labs/piperci/python-piperci.git"
      version: "master"
  noop_faas_ex:
    args:
      repository: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas"
      name: "executor"
      tag: "latest"
  noop_faas_gw:
    args:
      repository: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas"
      name: "gateway"
      tag: "latest"
...
...
```

#### Docs Config File

The value of `releases.${release_name_here}.components.docs.config` is a file containing configuration data feed into the docs builder located at `uri` with version `version`. The `docs` dictionary in this file lists each component that the docs builder downloads and uses. The docs builder is `python` based, and uses the `uri` passed in this file to pull a series of git repositories and/or files that it then feeds into `sphinx` for documentation rendering.

```
---
docs:
  - name: "piperci-picli"
    uri: "git@gitlab.com:dreamer-labs/piperci/piperci-picli.git"
    version: "master"
  - name: "piperci-gman"
    uri: "git@gitlab.com:dreamer-labs/piperci/piperci-gman.git"
    version: "master"
  - name: "python-piperci"
    uri: "git@gitlab.com:dreamer-labs/piperci/python-piperci.git"
    version: "master"
  - name: "piperci-noop-faas"
    uri: "git@gitlab.com:dreamer-labs/piperci/piperci-noop-faas.git"
    version: "master"
...
```
